FROM python:3
ENV PYTHONUNBUFFERED 1
RUN mkdir /FE-TKAI
WORKDIR /FE-TKAI
COPY requirements.txt /FE-TKAI/
RUN pip install -r requirements.txt
COPY . /FE-TKAI/

EXPOSE 5000
CMD ["python3", "./app.py"]

