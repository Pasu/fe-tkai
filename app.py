from flask import Flask, render_template, request, redirect, url_for, session
import json
import os
import random
from forms import JadwalForm, UserLoginForm
import requests
from datetime import datetime

app = Flask(__name__)
SECRET_KEY = os.urandom(32)
app.config['SECRET_KEY'] = SECRET_KEY

urlApiJadwal = "http://3.236.222.15:32045/api/" #api untuk jadwal
urlApiLogin = "http://3.236.222.15:32418/" #api untuk login
@app.route('/')
def landingPage():
	return redirect(url_for('listJadwal'))

@app.route('/home')
def listJadwal():
	if 'token' in session:
		print(session['token'])
		f = open('dataJadwal.json',)
		headers = {}
		headers["token"] = session['token']
		param = "?userId="+str(session['userId'])
		response = requests.get(urlApiJadwal+param, headers = headers)
		print(response.status_code)
		dataJadwal = []
		if response.status_code == 200:
			dataJadwal = response.json()
			print(dataJadwal)
			dataJadwal = dataJadwal['result']
		f.close()
		return render_template("list jadwal.html", dataJadwal=dataJadwal, jumlahData=len(dataJadwal))
	return redirect(url_for('userMasuk'))


@app.route('/buatJadwal', methods=('GET', 'POST'))
def buatJadwal():
	if 'token' in session:
		form = JadwalForm()
		if form.validate_on_submit():
			data = {}
			data["nama_matkul"] = request.form['nama_matkul']
			data["userId"] = session['userId']
			data["tanggal"] = request.form["tanggal"]
			data["jamMulai"] = request.form["jamMulai"]
			data["jamSelesai"] = request.form["jamSelesai"]
			headers = {}
			headers["token"] = session['token']
			response = requests.post(urlApiJadwal, data=data,headers = headers)
			print(response.status_code)
			return redirect(url_for('listJadwal'))
		return render_template("buat_jadwal.html", form = form, buatJadwal = True)
	return redirect(url_for('userMasuk'))

@app.route('/updateJadwal/<id>',methods=('GET','POST'))
def updateJadwal(id):
	if 'token' in session:
		headers = {}
		headers["token"] = session['token']
		form = JadwalForm()
		if form.validate_on_submit():
			data = {}
			data["nama_matkul"] = request.form['nama_matkul']
			data["userId"] = session['userId']
			data["tanggal"] = request.form["tanggal"]
			data["jamMulai"] = request.form["jamMulai"]
			data["jamSelesai"] = request.form["jamSelesai"]
			
			response = requests.put(urlApiJadwal+str(id)+"/", data=data, headers = headers)
			print(response)
			return redirect(url_for('listJadwal'))
		response = requests.get(urlApiJadwal+str(id)+"/",headers = headers)
		dataJSON = response.json()
		form['nama_matkul'].data = dataJSON['nama_matkul']
		print("nama_matkul")
		tanggalStrList = dataJSON['tanggal'].split("-")
		print(tanggalStrList)
		tanggal = datetime(int(tanggalStrList[0]),int(tanggalStrList[1]),int(tanggalStrList[2]))
		form['tanggal'].data = tanggal
		form['jamMulai'].data = datetime.strptime(dataJSON['jamMulai'], '%H:%M:%S')
		print("jamMulai masuk")
		form['jamSelesai'].data = datetime.strptime(dataJSON['jamSelesai'], '%H:%M:%S')
		return render_template("update_jadwal.html", form = form, buatJadwal = False, jadwal_id = id)
	return redirect(url_for('userMasuk'))

@app.route('/deleteJadwal/<id>')
def deleteJadwal(id):
	if 'token' in session:
		urlSementara = urlApiJadwal+str(id)+"/"
		headers = {}
		headers["token"] = session['token']
		response = requests.delete(urlSementara, headers = headers)
		print(response.status_code)
		return redirect(url_for('listJadwal'))
	return redirect(url_for('userMasuk'))


@app.route('/keluar')
def userKeluar():
	if 'token' in session:
		session.pop('token',None)
		session.pop('userId',None)
	return redirect(url_for('userMasuk'))

@app.route('/masuk', methods=('GET','POST'))
def userMasuk():
	form = UserLoginForm()
	if form.validate_on_submit():
		param = "?username="+request.form["username"]
		param += "&password="+request.form["password"]
		response = requests.get(urlApiLogin+param)
		data = response.json()
		print(data)
		if(data['token']):
			session["token"] = data['token']
			session["userId"] = data['id']
			return redirect(url_for('listJadwal'))
	return render_template("login.html",form=form)

if __name__ == "__main__":
    app.run(host='0.0.0.0')