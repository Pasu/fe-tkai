from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField
from wtforms.fields.html5 import DateField , TimeField
from wtforms.validators import DataRequired, Length
from wtforms import Form


class JadwalForm(FlaskForm):
    # Form untuk buat dan update jadwal
    nama_matkul = StringField(
        'Nama Matkul',
        [DataRequired(message='Nama Matkul tidak boleh kosong')]
    )
    tanggal = DateField('Tanggal',[DataRequired(message='Tanggal tidak boleh kosong')])
    jamMulai = TimeField('Jam Mulai',[DataRequired(message='Jam Mulai tidak boleh kosong')])
    jamSelesai = TimeField('Jam Selesai',[DataRequired(message='Jam Selesai tidak boleh kosong')])

class UserLoginForm(FlaskForm):
    username = StringField('Username', [DataRequired(message='Username tidak boleh kosong')])
    password = PasswordField("Password", [DataRequired(message='Password tidak boleh kosong')])